//
//  Project.swift
//  JiraTask
//
//  Created by Elena Geller on 12.05.20.
//  Copyright © 2020 Elena Geller. All rights reserved.
//

import Foundation

struct Project: Codable, Identifiable, Hashable , Equatable {
    static func == (lhs: Project, rhs: Project) -> Bool {
        rhs.id == lhs.id
    }
    
    func hash(into hasher: inout Hasher) {
        hasher.combine(id)
        hasher.combine(key)
        hasher.combine(name)
        hasher.combine(tasks)
        hasher.combine(avatarUrls)
    }
    
    
  var key: String
  var name: String
  var id: String
  var tasks: [ProjectTask]? = []
  var avatarUrls: AvatarUrls
  
}

struct AvatarUrls: Codable, Hashable {
    var m48 : String
    var m24 : String
    
    private enum CodingKeys : String, CodingKey {
        case m48 = "48x48", m24 = "24x24"
    }
    
    func hash(into hasher: inout Hasher) {
            hasher.combine(m48)
            hasher.combine(m24)
       
        }
}

struct ProjectSpentTime: Codable, Identifiable {
    var time : Int
    var avatar : String?
    var id = UUID().uuidString
    
    init(time:Int, avatar: String?){
        self.time = time
        self.avatar = avatar
    }
}
