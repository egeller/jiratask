//
//  UndefinedWorklog.swift
//  JiraTask
//
//  Created by Elena Geller on 21.05.22.
//  Copyright © 2022 Elena Geller. All rights reserved.
//

import Foundation
import Realm
import RealmSwift

class UndefinedWorklogObject: Object, Identifiable {
    
    
    @Persisted(primaryKey: true) var _id: String
    @Persisted var begin: Date
    @Persisted var end: Date?
    @Persisted var isSent: Bool = false
    @Persisted var account: String
    
    
}

class UndefinedWorklog {
    var id: String
    var begin: Date
    var end: Date?
    var isSent: Bool = false
    var account: String = ""
    
    init(from worklogObject: UndefinedWorklogObject){
        id = worklogObject._id
        begin = worklogObject.begin
        end = worklogObject.end
        isSent = worklogObject.isSent
        account = worklogObject.account
    }
}
