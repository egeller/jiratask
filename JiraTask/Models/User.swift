//
//  User.swift
//  JiraTask
//
//  Created by Elena Geller on 23.04.22.
//  Copyright © 2022 Elena Geller. All rights reserved.
//

import Foundation

struct User: Codable, Identifiable, Hashable {
    let accountId: String
    var id: String { accountId }
    let emailAddress: String
    let displayName: String
    
    func hash(into hasher: inout Hasher) {
        hasher.combine(id)
        hasher.combine(accountId)
        hasher.combine(emailAddress)
        hasher.combine(displayName)
        
    }
}
