//
//  JiraLogin.swift
//  JiraTask
//
//  Created by Elena Geller on 13.04.20.
//  Copyright © 2020 Elena Geller. All rights reserved.
//

import Foundation
struct JiraLogin: Codable, Identifiable{
    
  var login: String;
  var password: String;
  var serverUrl: String;
  var name: String;
  var id: String;
  
}
