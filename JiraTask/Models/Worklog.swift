//
//  Worklog.swift
//  JiraTask
//
//  Created by Elena Geller on 23.04.22.
//  Copyright © 2022 Elena Geller. All rights reserved.
//

import Foundation


struct Worklog: Codable, Identifiable, Hashable {
    
    
    var id: String
    var issueId: String
    var timeSpentSeconds:Int
    var created: String
    var updated: String
    var started: String
    var author: User
    
    func hash(into hasher: inout Hasher) {
        hasher.combine(id)
        hasher.combine(issueId)
        hasher.combine(timeSpentSeconds)
        hasher.combine(created)
        hasher.combine(started)
        hasher.combine(author)
    }
    
}

struct UpdatedWorklogsGroup: Codable {
    var since: Int
    var values: [UpdatedWorklog]
}

struct UpdatedWorklog: Codable, Identifiable {
    var worklogId : Int
    var id: Int {
        return worklogId
    }
}

