//
//  Task.swift
//  JiraTask
//
//  Created by Elena Geller on 07.11.20.
//  Copyright © 2020 Elena Geller. All rights reserved.
//

import Foundation

struct Task: Codable, Identifiable, Hashable {
    
    var key: String;
    //var summary: String;
    var id: String;
    var worklogs: [Worklog]? = []
    var fields : TaskFields?
    func hash(into hasher: inout Hasher) {
            hasher.combine(id)
            hasher.combine(key)
        hasher.combine(worklogs)
        hasher.combine(fields)
        
        }
    
}

struct UpdatedTasks : Codable {
    var issues : [Task] = []
    var startAt: Int
    var total: Int
}

struct TaskFields: Codable, Hashable {
    //var id = UUID().uuidString
    var project: Project
    func hash(into hasher: inout Hasher) {
            hasher.combine(project)
         
        }
    
}

struct ProjectTask : Codable, Identifiable, Hashable {
    var key: String;
    //var summary: String;
    var id: String;
    var worklogs: [Worklog]? = []
    var project_id : String
    init(task: Task) {
        id = task.id
        key = task.key
        worklogs = task.worklogs
        project_id = task.fields?.project.id ?? "0"
        
    }
    
    func hash(into hasher: inout Hasher) {
        hasher.combine(id)
        hasher.combine(key)
        hasher.combine(worklogs)
        hasher.combine(project_id)
       
    }
    
    
}

struct JirTasksResponse: Codable {
    var expand: String
    var issues: [Task]
}
