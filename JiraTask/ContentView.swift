//
//  ContentView.swift
//  JiraTask
//
//  Created by Elena Geller on 04.04.20.
//  Copyright © 2020 Elena Geller. All rights reserved.
//

import SwiftUI
import LocalAuthentication

struct ContentView: View {
    @State private var selection = 0
    @State private var isUnlocked = false
    @State private var passwordSet = false
    @ObservedObject var securitySettingsViewModel: SecuritySettingsViewModel
    @ObservedObject var authManager = AuthManager.shared
    @ObservedObject var jiraSettingsViewModel: JiraSettingsViewModel
    
    
    var body: some View {
        TabView(selection: $selection){
            VStack(){
                if !securitySettingsViewModel.isBiometricNeeded {
                    AccountsView(jiraSettingsViewModel: jiraSettingsViewModel)
                } else {
                    if  authManager.isAutheticated {
                    AccountsView(jiraSettingsViewModel: jiraSettingsViewModel)
                } else {
                    LoginView()
                }
                }
                
            }
            .font(.title)
            .tabItem {
                VStack {
                    Image(systemName: "house.fill")
                    Text("Home")
                }
            }
            .tag(0)
            SettingsView(securitySettingsViewModel: securitySettingsViewModel, jiraSettingsViewModel: jiraSettingsViewModel)
                .font(.title)
                .tabItem {
                    VStack {
                        Image(systemName: "gear")
                        Text("Settings")
                    }
                }
                .tag(1)
            InfoView()
                .font(.title)
                .tabItem {
                    VStack {
                        Image(systemName: "info.circle.fill")
                        Text("Info")
                    }
                }
                .tag(2)
        }
        .task {
            if securitySettingsViewModel.isBiometricNeeded {
            do {
            _ = try  await authManager.authenticate()
            } catch {
                //
            }
            }
        }
    }
    
    
    
    func authenticate() {
        let context = LAContext()
        var error: NSError?
        if securitySettingsViewModel.isBiometricNeeded {
            // check whether biometric authentication is possible
            if context.canEvaluatePolicy(.deviceOwnerAuthentication, error: &error) {
                // it's possible, so go ahead and use it
                let reason = NSLocalizedString("We need to unlock your data.", comment: "")
                
                context.evaluatePolicy(.deviceOwnerAuthentication, localizedReason: reason) { success, authenticationError in
                    // authentication has now completed
                    DispatchQueue.main.async {
                        if success {
                            self.isUnlocked = true
                            // authenticated successfully
                        } else {
                            
                            // there was a problem
                        }
                    }
                }
            } else {
                self.isUnlocked = true
                // no biometrics
            }
            
        } else {
            self.isUnlocked = true;
        }


    }
}

struct ContentView_Previews: PreviewProvider {
    static var previews: some View {
        ContentView(securitySettingsViewModel: SecuritySettingsViewModel(), jiraSettingsViewModel: JiraSettingsViewModel())
    }
}
