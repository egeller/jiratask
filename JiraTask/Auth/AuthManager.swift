//
//  AuthManager.swift
//  JiraTask
//
//  Created by Elena Geller on 02.06.22.
//  Copyright © 2022 Elena Geller. All rights reserved.
//

import Foundation
import LocalAuthentication

@MainActor
class AuthManager: ObservableObject {
    
    static let shared = AuthManager()
    
    @Published var isAutheticated = false
    
    ///@see https://danielsaidi.com/blog/2022/04/29/extending-local-authentication-with-async-support
    func authenticate() async throws -> Bool {
        return try await withCheckedThrowingContinuation { cont in
            let reason = NSLocalizedString("We need to unlock your data.", comment: "")
            LAContext().evaluatePolicy(.deviceOwnerAuthentication, localizedReason: reason) { result, error in
                if let error = error { return cont.resume(throwing: error) }
                DispatchQueue.main.async {
                    self.isAutheticated = true
                }
                cont.resume(returning: result)
            }
        }
        
    }
    
}
