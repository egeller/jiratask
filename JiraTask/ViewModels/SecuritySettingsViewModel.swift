//
//  SecuritySettingsViewModel.swift
//  JiraTask
//
//  Created by Elena Geller on 18.04.20.
//  Copyright © 2020 Elena Geller. All rights reserved.
//

import Foundation
import SwiftUI
import Combine

public class SecuritySettingsViewModel: ObservableObject{
    public let willChange = PassthroughSubject<SecuritySettingsViewModel, Never>()
    private let settingHelper = SettingsHelper();
    
    @Published var isBiometricNeeded: Bool  {
        didSet {
            willChange.send(self)
        }
    }
    
    init(){
        isBiometricNeeded = settingHelper.isBiometricEnabled()
    }
    
    func readBiometricNeeded(){
        isBiometricNeeded = settingHelper.isBiometricEnabled()
    }
    
    func saveBiometricNeeded(){
        settingHelper.enableBiometric(toEnable: isBiometricNeeded)
        
    }
    
    
}
