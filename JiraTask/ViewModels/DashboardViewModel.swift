//
//  DashboardViewModel.swift
//  JiraTask
//
//  Created by Elena Geller on 01.05.22.
//  Copyright © 2022 Elena Geller. All rights reserved.
//

import Foundation
import SwiftUI
import Combine


class DashboardViewModel: ObservableObject {
    
    public let willChange = PassthroughSubject<DashboardViewModel, Never>()
    
    
    @Published var projects: [Project] = []
    
    @Published var projectsSpentSeconds: [ String : Int ] = [:]
    
    @Published var projectSpentTimes: [String : ProjectSpentTime] = [:]
    
    let dummyAccount = JiraLogin(login: "", password: "", serverUrl: "", name: "", id: "0")
    
    let api = ServerApi.shared
    @Published var activeAccount: JiraLogin?  {
        didSet {
            willChange.send(self)
        }
    }
    
    init(activeAccount : JiraLogin) {
        self.activeAccount = activeAccount
    }
    
    @MainActor
    func getProjectsSpentTimes() async -> [String : ProjectSpentTime ] {
        api.setAccount(activeAccount ?? dummyAccount )
        //var projects: [Project] = []
        var user: User
        var tasks: UpdatedTasks
        var wlogs: [UpdatedWorklog] = []
        var wls: [Worklog] = []
        do {
            print("--dashboard--")
            self.projects = try await api.getProjects()
            //print(projects)
        } catch {
            print("err")
        }
        
        do {
            print("--dashboard 2--")
            user = try await api.getMyself()
            print(user)
        } catch {
            print("err")
        }
        
        do {
            print("projects \(projects)")
            print("--tasks--")
            tasks = try await api.getWorkedTasksForToday()
            print(tasks)
            do {
                print("--worklogsids--")
                wlogs = try await api.worklogsForDay()
                print("worklogsid \(wlogs)")
                var retVal:  [String : ProjectSpentTime ]   = [:]
                let worklogsIdsAsArray = wlogs.map{ $0.id}
                print("worklog ids as strung \(worklogsIdsAsArray)")
                do {
                    print("--worklogs--")
                    wls = try await api.getWorklogList(listOfIds: worklogsIdsAsArray)
                    print("worklogs today \(wls)")
                    for wl in wls {
                        let task: Task? = tasks.issues.filter { updatedTask in
                            updatedTask.id == wl.issueId
                        }.first
                        let projectKey = task?.fields?.project.key ?? ""
                        if projectKey == "" {
                            continue
                        }
                        let project: Project? = projects.filter { proj in
                            proj.key == projectKey
                        }.first
                        
                        print("projectKey \(projectKey)  worklog \(wl.id) task \(task?.id ?? "?")")
                        //print(task)
                        if retVal[projectKey] != nil {
                            
                            retVal[projectKey]!.time += wl.timeSpentSeconds
                        } else {
                            let avatar = try await api.getSVGStringForImage(url: project?.avatarUrls.m24 ?? "")
                            var projectSpentTime = ProjectSpentTime(time: 0, avatar: avatar)
                            retVal[projectKey] = projectSpentTime
                            retVal[projectKey]?.time = wl.timeSpentSeconds
                        }
                        
                        
                    }
                    print("retavl \(retVal)")
                    self.projectSpentTimes = projectSpentTimes
                    return retVal
                } catch {
                    print("err wlogs")
                }
            } catch {
                print("err wlogs")
            }
        } catch {
            print("err tasks")
        }
        return [:]
    }
    
   
    
    
    
}
