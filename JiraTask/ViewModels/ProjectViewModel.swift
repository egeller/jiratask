//
//  ProjectViewModel.swift
//  JiraTask
//
//  Created by Elena Geller on 07.11.20.
//  Copyright © 2020 Elena Geller. All rights reserved.
//

import Foundation

@MainActor
class ProjectViewModel: ObservableObject {
    
   @Published var tasks: [Task] = []
    
    
    @Published var project: Project
    
    let api = ServerApi.shared
    
    func getTasks() async throws -> [Task] {
        print("tasks")
        tasks = try await api.getProjectTasks(project: project)
        return tasks
    }
    
    init(project: Project) {
        self.project = project
    }
}
