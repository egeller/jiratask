//
//  JiraSettingsViewModel.swift
//  JiraTask
//
//  Created by Elena Geller on 02.05.20.
//  Copyright © 2020 Elena Geller. All rights reserved.
//

import Foundation
import SwiftUI
import Combine

class JiraSettingsViewModel: ObservableObject{
    public let willChange = PassthroughSubject<JiraSettingsViewModel, Never>()
    private let settingHelper = SettingsHelper();
    let accountsHolder = AccountsHolder.shared

    @Published var projects: [Project] {
        didSet {
            willChange.send(self)
        }
    }
 
    @Published var testSuccessfull: Bool{
        didSet {
            willChange.send(self)
        }
    }
    
    @Published var testRun: Bool{
        didSet {
            willChange.send(self)
        }
    }

    @Published var testFails: Bool{
        didSet {
            willChange.send(self)
        }
    }
    
    @Published var testVal: Bool{
           didSet {
               willChange.send(self)
           }
       }

    @Published var jiraAccounts: [JiraLogin]  {
        didSet {
            willChange.send(self)
        }
    }
    
    @Published var activeAccount: JiraLogin?  {
        didSet {
            willChange.send(self)
        }
    }
    
    
    init(){
        jiraAccounts = settingHelper.readSettings()
        
        projects = [];
        testSuccessfull = false;
        testRun = false;
        testFails = false;
        testVal = true;
        
        accountsHolder.accounts = jiraAccounts
    }
    
    func refreshAccounts() {
        jiraAccounts = settingHelper.readSettings()
    }
    
    func setActiveAccount(account:JiraLogin){
        activeAccount = account
    }
    
    func addAccount(account: JiraLogin){
        jiraAccounts.append(account);
        accountsHolder.accounts = jiraAccounts
        settingHelper.saveSettings(settings: jiraAccounts);
        //self.testRun = false;
        //self.testSuccessfull = false;
        DispatchQueue.main.async{
            self.testRun = false;
            self.testSuccessfull = false;
            self.testFails = false;
            
        }
    }
    
    func removeAccount(account: JiraLogin){
        var reducedAccounts: [JiraLogin]  = [];
        for entry in jiraAccounts{
            if entry.id != account.id{
                reducedAccounts.append(entry);
            }
        }
        jiraAccounts = reducedAccounts;
        accountsHolder.accounts = jiraAccounts
        settingHelper.saveSettings(settings: reducedAccounts);
    }
    
    func testAccount(account: JiraLogin){
        testRun = false;
        testFails = false;
        print(self.jiraAccounts)
        testSuccessfull = false;
        let pathHelper = RequestPathHelper();
        pathHelper.readPathJson();
        let projectsPathData = pathHelper.readPathParametersFor(reqName: "projects");
        print(projectsPathData)
        
        let url = URL(string: account.serverUrl + projectsPathData.uri)
        print(url ?? "url")
        guard let requestUrl = url else { fatalError() }
        // Create URL Request
        var request = URLRequest(url: requestUrl)
        // Specify HTTP Method to use
        request.httpMethod = projectsPathData.method
        
        let paramsStr = account.login + ":" + account.password
        //print(paramsStr);
        let userdata = paramsStr.data(using: String.Encoding.utf8)
        //print(userdata!);
        
        let basic = userdata?.base64EncodedString(options: Data.Base64EncodingOptions(rawValue: 0))
        print("basic", basic!);
        
        
        request.setValue("application/json", forHTTPHeaderField: "Accept")
        request.setValue("Basic " + basic!, forHTTPHeaderField: "Authorization")
        // Send HTTP Request
        let task = URLSession.shared.dataTask(with: request) { (data, response, error) in
            
            // Check if Error took place
            if let error = error {
                print("Error took place \(error)")
                DispatchQueue.main.async{
                    self.testRun = true
                    self.testSuccessfull = false
                    self.testFails = true
                    
                }
                
                return
            }
            
            // Read HTTP Response Status code
            if let response = response as? HTTPURLResponse {
                print("Response HTTP Status code: \(response.statusCode)")
            }
            
            // Convert HTTP Response Data to a simple String
            if let data = data {
                if let decodedResponse = try? JSONDecoder().decode([Project].self, from: data) {
                    // we have good data – go back to the main thread
                    DispatchQueue.main.async {
                        // update our UI
                        print(decodedResponse)
                        self.projects = decodedResponse;
                        self.testSuccessfull = true;
                        self.testRun = true;
                        self.testFails = false;
                        
                    }

                    // everything is good, so we can exit
                    
                    return
                }
            }
            
            // if we're still here it means there was a problem
            print("Fetch failed: \(error?.localizedDescription ?? "Unknown error")")
            //self.testSuccessfull = false;
            //self.testRun = true;
            DispatchQueue.main.async {
                
                self.testSuccessfull = false;
                self.testFails = true;
                self.testRun = true;
                
            }
            
        }
        task.resume()
        
        
    }
    
}
