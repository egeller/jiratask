//
//  ProjectsViewModel.swift
//  JiraTask
//
//  Created by Elena Geller on 28.05.22.
//  Copyright © 2022 Elena Geller. All rights reserved.
//

import Foundation

@MainActor
class ProjectsViewModel: ObservableObject {
    
   @Published var projects: [Project] = []
    
    let api = ServerApi.shared
    
    func getProjects() async throws -> [Project] {
        print("projects")
        projects = try await api.getProjects()
        return projects
    }
}
