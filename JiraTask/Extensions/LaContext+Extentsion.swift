//
//  LaContext+Extentsion.swift
//  JiraTask
//
//  Created by Elena Geller on 02.06.22.
//  Copyright © 2022 Elena Geller. All rights reserved.
//

import Foundation
import LocalAuthentication

@available(iOS 15.0, macOS 12.0, *)
extension LAContext {
    
    func evaluatePolicy(_ policy: LAPolicy, localizedReason reason: String) async throws -> Bool {
        return try await withCheckedThrowingContinuation { cont in
            LAContext().evaluatePolicy(policy, localizedReason: reason) { result, error in
                if let error = error { return cont.resume(throwing: error) }
                cont.resume(returning: result)
            }
        }
    }
}
