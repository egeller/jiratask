//
//  DateExtension.swift
//  JiraTask
//
//  Created by Elena Geller on 01.05.22.
//  Copyright © 2022 Elena Geller. All rights reserved.
//

import Foundation

/// @see  https://www.codingem.com/swift-date-to-milliseconds/
extension Date {
    var millisecondsSince1970: Int64 {
        Int64((self.timeIntervalSince1970 * 1000.0).rounded())
    }
    
    init(milliseconds: Int64) {
        self = Date(timeIntervalSince1970: TimeInterval(milliseconds) / 1000)
    }
}
