//
//  FormatHelper.swift
//  JiraTask
//
//  Created by Elena Geller on 07.05.22.
//  Copyright © 2022 Elena Geller. All rights reserved.
//

import Foundation

class FormatHelper {
    
    static func formatSpentTime(_ time: Double) -> String {
        let hours = time/3600.0
        return String(format: "%.2f", hours)
        
    }
}
