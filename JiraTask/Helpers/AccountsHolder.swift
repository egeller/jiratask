//
//  AccountsHolder.swift
//  JiraTask
//
//  Created by Elena Geller on 02.06.22.
//  Copyright © 2022 Elena Geller. All rights reserved.
//

import Foundation
import SwiftUI

class AccountsHolder: ObservableObject {
    
    static let shared = AccountsHolder()
    
    @Published var accounts: [JiraLogin] = []
}
