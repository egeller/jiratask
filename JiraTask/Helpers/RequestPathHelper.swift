//
//  RequestPathHelper.swift
//  JiraTask
//
//  Created by Elena Geller on 02.05.20.
//  Copyright © 2020 Elena Geller. All rights reserved.
//

import Foundation


import Foundation

struct JiraUri: Codable {
    let name: String
    let uri: String
    let method: String
  
}


class RequestPathHelper{
    
    var jiraUriCollection : [JiraUri] = []
    // egeller
    // token oW8xGAxOSi6C5ZM36HZ92C8A
    
    
    func readPathJson(){
        if let jsonURL = Bundle.main.url(forResource: "jiraPath", withExtension: "json") {
            do {
                let jsonData = try Data(contentsOf: jsonURL)
                print(jsonData)
                do{
                    let jsonDecoder = JSONDecoder()
                    jiraUriCollection = try jsonDecoder.decode([JiraUri].self, from: jsonData)
                    print(jiraUriCollection)
                    
                }catch{
                    print("the file can't be parsed")
                }
                
            } catch {
                print("The file could not be loaded")
            }
            
           
        }
        
    }
    
    func readPathParametersFor(reqName : String)->JiraUri{
        var retValue = JiraUri(name: "", uri: "", method: "");
        for jiraUri in jiraUriCollection{
            if jiraUri.name == reqName {
                retValue = jiraUri;
            }
        }
        return retValue;
    }
}
