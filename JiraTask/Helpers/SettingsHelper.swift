//
//  JiraSettings.swift
//  JiraTask
//
//  Created by Elena Geller on 14.04.20.
//  Copyright © 2020 Elena Geller. All rights reserved.
//

import Foundation
import CryptoKit

class SettingsHelper: ObservableObject {
    
    let settingsKey = "jira_settings";
    let keyStore = NSUbiquitousKeyValueStore();
    let biometricEnableKey = "jira_biometric_enable";
    let faceIDEnableKey = "jira_faceId_enable";
    let appPasswordKey = "jira_appPassword_key"
    
    /*
     verwandelt Array von jiraSettings Objekten in json-String
     */
    func encodeSettings(settings: [JiraLogin])->String{
        var retValue = "";
        var loginData:[JiraLogin] = [];
        for setting in settings{
            loginData.append(setting);
            
        }
        let encoder = JSONEncoder()
        encoder.outputFormatting = .prettyPrinted
        do {
            let data = try  encoder.encode(loginData)
            retValue = (String(data: data, encoding: .utf8)!)
        } catch {
            print("Not Codable?")
        }
        
        
        return retValue;
    }
    
    /*
        liefert key für Jira Account
     */
    func setJiraLoginKey(settings: JiraLogin)->String{
        let login = settings.login;
        let serverUrl = settings.serverUrl;
        return login + "#" + serverUrl;
    }
    
    func setJiraLoginId(settings: JiraLogin)->Int{
        return Int(Date().timeIntervalSince1970);
    }
    
    /*
     verwandelt json-String in Array von jiraSettings Objekten
    */
    func decodeSettings(encodedSettings:String)->[JiraLogin]{
        //var retValue:[JiraLogin] = [];
        var loginData:[JiraLogin] = [];
        let decoder = JSONDecoder();
        do{
            loginData = try decoder.decode([JiraLogin].self, from: encodedSettings.data(using: .utf8)! )
        }catch{
            print("Not decodable")
        }
        /*
        for login in loginData{
            var jiraSettings = JiraSettings();
            jiraSettings.setData(jiralogin: login);
            retValue.append(jiraSettings);
        }*/
        
        
        return loginData;
        
    }
    
    /*
     Speichert settings in iCloud
    */
    func saveSettings(settings: [JiraLogin]){
        keyStore.set(encodeSettings(settings: settings), forKey: settingsKey);
        keyStore.synchronize();
    }
    
    /*
     liest settings von iCloud
    */
    func readSettings()->[JiraLogin]{
        var retValue:[JiraLogin] = [];
        let encodedSettings = keyStore.string(forKey: settingsKey);
        if(encodedSettings != nil && encodedSettings != ""){
          retValue = decodeSettings(encodedSettings:encodedSettings!);
        }
        
        
        return retValue;
    }
    //biometric identification, enabled/disabled from user
    
    func isBiometricEnabled()->Bool{
        print("read isBiometricEnabled, \(keyStore.bool(forKey: biometricEnableKey))")
        
        return keyStore.bool(forKey: biometricEnableKey);
    }
    
    func isFaceIdEnabled()->Bool{
        return keyStore.bool(forKey: faceIDEnableKey);
    }
    
    func enableBiometric(toEnable:Bool){
        print("toEnable, \(toEnable)")
        keyStore.set(toEnable, forKey: biometricEnableKey);
        keyStore.synchronize();
    }
    
    func enableFaceIDLogin(toEnable:Bool){
        keyStore.set(toEnable, forKey: faceIDEnableKey);
        keyStore.synchronize();
    }
    
    func encryptPassword(appPassword: String)->String{
        let inputData = Data(appPassword.utf8);
        let hashed = SHA256.hash(data: inputData);
        return hashed.description;
        
    }
    //AppPassword
    /*
     setzt App Password
    */
    func setAppPassword(appPassword:String){
        keyStore.set(encryptPassword(appPassword: appPassword), forKey: appPasswordKey);
        keyStore.synchronize();
        
    }
    
    /*
     überprüft, ob App-Passwort gesetzt ist
    */
    func isAppPasswordSet()->Bool{
        var retValue = true;
        let encryptedAppPassword = keyStore.string(forKey: appPasswordKey);
        if(encryptedAppPassword == nil || encryptedAppPassword == ""){
            retValue = false;
        }
        
        return retValue;
    }
    
    /*
     vergleicht das gespeicherte Password mit dem Eingetragenen
    */
    func checkPassword(userInput:String)->Bool{
        let encryptedAppPassword = keyStore.string(forKey: appPasswordKey);
        print(encryptPassword(appPassword: userInput));
        print(encryptedAppPassword ?? "");
        return encryptPassword(appPassword: userInput) == encryptedAppPassword;
    }
    
    
}
