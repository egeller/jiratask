//
//  ServerApi.swift
//  JiraTask
//
//  Created by Elena Geller on 20.04.22.
//  Copyright © 2022 Elena Geller. All rights reserved.
//

import Foundation
import Alamofire
import UIKit
import WebKit
import SwiftUI

class ServerApi: ObservableObject {
    
    static let shared = ServerApi()
    
    var account: JiraLogin = JiraLogin(login: "", password: "", serverUrl: "", name: "", id: "0")
    
    var user: User = User(accountId: "", emailAddress: "", displayName: "")
    
    func setAccount(_ login : JiraLogin) {
       account = login
    }
    
    @MainActor
    func getImage(url: String)  -> UIImage? {
        let url = URL(string: url)
        //print(url ?? "url")
        guard let requestUrl = url else { fatalError() }
        // Create URL Request
        let request = URLRequest(url: requestUrl)
        let web = WKWebView()
        web.load(request)
        
        let renderer =  UIGraphicsImageRenderer(size: web.bounds.size)
        let image = renderer.image { ctx in
            web.drawHierarchy(in: web.bounds, afterScreenUpdates: true)
        }
        
        return image
    }
    
    func getSVGStringForImage(url: String) async throws -> String? {
        try await withUnsafeThrowingContinuation { continuation in
            
            AF.request(url,method: .get, headers:  HTTPHeaders(setHeaders())).response { response in
                print(url)
                if let data = response.data {
                    
                        print(data)
                        let svgData =  "\(String(decoding: response.data ?? Data(), as: UTF8.self))"
                            
                      
                        //print("projectimage! \(image)")
                        print("\(String(decoding: response.data ?? Data(), as: UTF8.self))")
                        continuation.resume(returning: svgData)
                        return
                    
                }
                if let err = response.error {
                    continuation.resume(throwing: err)
                    return
                }
            }
        }
    }
    
    func getSVGImage(url: String) async throws -> SVGWebView? {
        try await withUnsafeThrowingContinuation { continuation in
            
            AF.request(url,method: .get, headers:  HTTPHeaders(setHeaders())).response { response in
                //print(url)
                if let data = response.data {
                    do {
                        print(data)
                        let svgData = "\(String(decoding: response.data ?? Data(), as: UTF8.self))"
                        let svg = SVGWebView(svg: svgData)
                            
                      
                        //print("projectimage! \(image)")
                        //print("\(String(decoding: response.data ?? Data(), as: UTF8.self))")
                        continuation.resume(returning: svg)
                        return
                    } catch  {
                        continuation.resume(returning : nil)
                        return
                    }
                    
                    
                }
                if let err = response.error {
                    continuation.resume(throwing: err)
                    return
                }
            }
        }
        
    }
    
    
    /// finds projects in jira account where the user has access
    func getProjects() async throws -> [Project] {
        
        try await withUnsafeThrowingContinuation{ continuation in
            let pathHelper = RequestPathHelper();
            pathHelper.readPathJson();
            let projectsPathData = pathHelper.readPathParametersFor(reqName: "projects");
            //print(projectsPathData)
            let pathmethod = projectsPathData.method
            let url = account.serverUrl + projectsPathData.uri
            let method = resolveMethod(method: pathmethod)
            
            AF.request(url, method: method, headers: HTTPHeaders(setHeaders()))
                .responseDecodable(of: [Project].self, decoder: JSONDecoder()){ response in
                    if let projects = response.value {
                        print(projects)
                        continuation.resume(returning: projects)
                        return
                    }
                    if let err = response.error {
                        print(err)
                        continuation.resume(throwing: err)
                        return
                    }
                    
                
            }
        }
       
    }
    
    func getProjectTasks(project: Project) async throws -> [Task] {
        try await withUnsafeThrowingContinuation{ continuation in
            let pathHelper = RequestPathHelper();
            pathHelper.readPathJson();
            let tasksPathData = pathHelper.readPathParametersFor(reqName: "tasks");
            //print(projectsPathData)
            let pathmethod = tasksPathData.method
            let url = account.serverUrl + tasksPathData.uri
            let method = resolveMethod(method: pathmethod)
            
            let jql =  "project=\(project.key)"
            
            print("tasks url \(url)  \(jql)")
            
            let parameters: Parameters = [
                "jql": jql
                ]
            
            AF.request(
                url,
                method: method,
                parameters: parameters,
                headers: HTTPHeaders(setHeaders()))
            .responseDecodable(of: JirTasksResponse.self, decoder: JSONDecoder()){ response in
                if let tasks = response.value?.issues {
                        print("tasks \(tasks)")
                    continuation.resume(returning: tasks)
                        return
                    }
                    if let err = response.error {
                        print(err)
                        continuation.resume(throwing: err)
                        return
                    }
                    
                
            }
        }
     
    }
    
    func getProjectTasksByProjectKey(projectKey: String) async throws -> [Task] {
        try await withUnsafeThrowingContinuation{ continuation in
            let pathHelper = RequestPathHelper();
            pathHelper.readPathJson();
            let tasksPathData = pathHelper.readPathParametersFor(reqName: "tasks");
            //print(projectsPathData)
            let pathmethod = tasksPathData.method
            let url = account.serverUrl + tasksPathData.uri
            let method = resolveMethod(method: pathmethod)
            
            let jql =  "project=\(projectKey)"
            
            print("tasks url \(url)  \(jql)")
            
            let parameters: Parameters = [
                "jql": jql
                ]
            
            AF.request(
                url,
                method: method,
                parameters: parameters,
                headers: HTTPHeaders(setHeaders()))
            .responseDecodable(of: JirTasksResponse.self, decoder: JSONDecoder()){ response in
                if let tasks = response.value?.issues {
                        print("tasks \(tasks)")
                    continuation.resume(returning: tasks)
                        return
                    }
                    if let err = response.error {
                        print(err)
                        continuation.resume(throwing: err)
                        return
                    }
                    
                
            }
        }
     
    }
    
    /// setd headers for request
    func setHeaders() -> [String : String] {
        let basic = getBasic();
        let headers = ["Authorization": "Basic \(basic)",
                       "Accept" : "application/json"]
        return headers
    }
    
    func resolveMethod(method: String) -> HTTPMethod {
        switch method {
        case "GET" :
            return HTTPMethod.get
        case "POST" :
            return HTTPMethod.post
        default:
            return HTTPMethod.post
        }
            
    }
    
    /// builds basic authorisation string
    func getBasic() -> String {
        let paramsStr = account.login + ":" + account.password
        //print(paramsStr);
        let userdata = paramsStr.data(using: String.Encoding.utf8)
        //print(userdata!);
        
        let basic = userdata?.base64EncodedString(options: Data.Base64EncodingOptions(rawValue: 0)) ?? ""
        //print("basic", basic)
        return basic
        
    }
    
    /// finds "me" (logged in user)
    func getMyself() async throws -> User {
        try await withUnsafeThrowingContinuation{ continuation in
            let pathHelper = RequestPathHelper();
            pathHelper.readPathJson();
            let myselfPathData = pathHelper.readPathParametersFor(reqName: "myself");
            //print(myselfPathData)
            let pathmethod = myselfPathData.method
            let url = account.serverUrl + myselfPathData.uri
            let method = resolveMethod(method: pathmethod)
            
            AF.request(url, method: method, headers: HTTPHeaders(setHeaders()))
                .responseDecodable(of: User.self, decoder: JSONDecoder()){ response in
                    if let user = response.value {
                        self.user = user
                        continuation.resume(returning: user)
                        return
                    }
                    if let err = response.error {
                        continuation.resume(throwing: err)
                        return
                    }
               
            }
        }
        
    }
    
    /// tasks changed today
    func getWorkedTasksForToday() async throws -> UpdatedTasks {
        try await withUnsafeThrowingContinuation{ continuation in
            let pathHelper = RequestPathHelper();
            pathHelper.readPathJson();
            let tasksPathData = pathHelper.readPathParametersFor(reqName: "taskschangedtoday");
            //print(tasksPathData)
            let pathmethod = tasksPathData.method
            let uri = String(tasksPathData.uri)
            let formatter = DateFormatter()
            formatter.dateFormat = "yyyy-MM-dd"
            let dateAsString  = formatter.string(for: Date())

            let url = account.serverUrl + uri
            let jql =  "updated>=\(dateAsString ?? "")"
            
            //print("tasks url \(url)  \(jql)")
            let method = resolveMethod(method: pathmethod)
            let parameters: Parameters = [
                "jql": jql
                ]
            AF.request(url,
                       method: method,
                       parameters: parameters,
                       //encoding: URLEncoding.queryString,
                       headers: HTTPHeaders(setHeaders()))
                .responseDecodable(of: UpdatedTasks.self, decoder: JSONDecoder()){ response in
                    //print("\(String(decoding: response.data ?? Data(), as: UTF8.self))")
                    if let updatedTasks = response.value {
                        continuation.resume(returning: updatedTasks)
                        return
                    }
                    if let err = response.error {
                        print(err.localizedDescription)
                        continuation.resume(throwing: err)
                        return
                    }
               
            }
        }
    }
    
    /// worklogs for today
    func worklogsForDay() async throws -> [UpdatedWorklog] {
        try await withUnsafeThrowingContinuation{ continuation in
            let pathHelper = RequestPathHelper();
            pathHelper.readPathJson();
            let formatter = DateFormatter()
            formatter.dateFormat = "yyyy-MM-dd"
            let dateAsString  = formatter.string(for: Date())

            let since = formatter.date(from: dateAsString ?? "2022-04-01")?.millisecondsSince1970
            let tasksPathData = pathHelper.readPathParametersFor(reqName: "worklogsupdatedtoday");
            //print(tasksPathData)
            let pathmethod = tasksPathData.method
            let uri = String(tasksPathData.uri)
            
            let url = account.serverUrl + uri
            
            //print("tasks url \(url)  \(jql)")
            let method = resolveMethod(method: pathmethod)
            let parameters: Parameters = [
                "since": since  ?? 0
                ]
            AF.request(url,
                       method: method,
                       parameters: parameters,
                       //encoding: URLEncoding.queryString,
                       headers: HTTPHeaders(setHeaders()))
            .responseDecodable(of: UpdatedWorklogsGroup.self, decoder: JSONDecoder()){ response in
                    //print("worklogs ids \(String(decoding: response.data ?? Data(), as: UTF8.self))")
                    if let value = response.value {
                        continuation.resume(returning: value.values)
                        return
                    }
                    if let err = response.error {
                        print(err.localizedDescription)
                        continuation.resume(throwing: err)
                        return
                    }
               
            }
        }
        
    }
    
    func getWorklogList(listOfIds: [Int]) async throws -> [Worklog] {
        try await withUnsafeThrowingContinuation{ continuation in
            let pathHelper = RequestPathHelper();
            pathHelper.readPathJson();
            let formatter = DateFormatter()
            formatter.dateFormat = "yyyy-MM-dd"
            let dateAsString  = formatter.string(for: Date())

            let since = formatter.date(from: dateAsString ?? "2022-04-01")?.millisecondsSince1970
            let tasksPathData = pathHelper.readPathParametersFor(reqName: "worklogsbyids");
            //print(tasksPathData)
            let pathmethod = tasksPathData.method
            let uri = String(tasksPathData.uri)
            
            let url = account.serverUrl + uri
            
            //print("tasks url \(url)  \(jql)")
            let method = resolveMethod(method: pathmethod)
            let parameters: Parameters = [
                "ids": listOfIds 
                ]
            AF.request(url,
                       method: method,
                       parameters: parameters,
                       encoding: JSONEncoding.default,
                       headers: HTTPHeaders(setHeaders()))
            .responseDecodable(of: [Worklog].self, decoder: JSONDecoder()){ response in
                    //print("worklogs? \(String(decoding: response.data ?? Data(), as: UTF8.self))")
                if let value = response.value?.filter { worklog in
                    worklog.author.accountId == self.user.accountId} {
                        continuation.resume(returning: value)
                        return
                    }
                    if let err = response.error {
                        print(err.localizedDescription)
                        continuation.resume(throwing: err)
                        return
                    }
               
            }
        }
      
    }
    
    
    
    func sendWorklog(issueKey: String, begin: Date, end: Date) async throws -> Bool {
        try await withUnsafeThrowingContinuation{ continuation in
            let pathHelper = RequestPathHelper();
            pathHelper.readPathJson();
            let RFC3339DateFormatter = DateFormatter()
            //RFC3339DateFormatter.locale = Locale(identifier: "en_US_POSIX")
            RFC3339DateFormatter.dateFormat = "yyyy-MM-dd'T'HH:mm:ss.SSSZ"
            let beginDateAsString  = RFC3339DateFormatter.string(for: begin)
             
            let timeSpentSeconds = end.timeIntervalSince1970 - begin.timeIntervalSince1970
            let tasksPathData = pathHelper.readPathParametersFor(reqName: "addworklog");
            //print(tasksPathData)
            let pathmethod = tasksPathData.method
            let uri = String(tasksPathData.uri)
            
            let url = account.serverUrl + uri.replacingOccurrences(of: "{issueIdOrKey}", with: issueKey)
            
            print("timespent url \(timeSpentSeconds)  \(beginDateAsString)")
            let method = resolveMethod(method: pathmethod)
            let parameters: Parameters = [
                "timeSpentSeconds":timeSpentSeconds,
                "started": beginDateAsString ?? ""
                ]
            AF.request(url,
                       method: method,
                       parameters: parameters,
                       encoding: JSONEncoding.default,
                       headers: HTTPHeaders(setHeaders()))
            .responseDecodable(of: Worklog.self, decoder: JSONDecoder()){ response in
                    print("worklog? \(String(decoding: response.data ?? Data(), as: UTF8.self))")
                        if let value = response.value {
                   
                        continuation.resume(returning: true)
                        return
                    }
                    if let err = response.error {
                        print(err.localizedDescription)
                        continuation.resume(returning: false)
                        return
                    }
               
            }
        }
      
    }
                   
                   
}
