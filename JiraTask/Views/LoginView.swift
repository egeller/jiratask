//
//  LoginView.swift
//  JiraTask
//
//  Created by Elena Geller on 04.04.20.
//  Copyright © 2020 Elena Geller. All rights reserved.
//

import SwiftUI

struct LoginView: View {
    
    let authManager = AuthManager.shared
    
    var body: some View {
        VStack {
            Spacer()
            Button(action: { self.login() }){
                HStack(){
                    Image("fingerprint")
                        .resizable()
                        .frame(width: 100, height: 100, alignment: .center)
               
                }
            }
            Spacer()
            }
            
        }
    
    func login() {
        async {
           try  await authManager.authenticate()
        }
    }
   
}

struct LoginView_Previews: PreviewProvider {
    static var previews: some View {
        LoginView()
    }
}
