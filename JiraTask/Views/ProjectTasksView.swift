//
//  ProjectTasksView.swift
//  JiraTask
//
//  Created by Elena Geller on 28.05.22.
//  Copyright © 2022 Elena Geller. All rights reserved.
//

import SwiftUI

struct ProjectTasksView: View {
    @ObservedObject  var projectViewModel: ProjectViewModel
    var body: some View {
        VStack{
            List(projectViewModel.tasks, id: \.self) { task in
                Text(task.key)
                    }
        }
        .task {
            do {
            _ = try await projectViewModel.getTasks()
            }catch {
                ///
            }
        }
    }
}
/*
struct ProjectTasksView_Previews: PreviewProvider {
    static var previews: some View {
        ProjectTasksView(projectViewModel:  ProjectViewModel(project: Project(
            key: "test",
            name: "test",
            id: "1003",
            avatarUrls: AvatarUrls[]
            
        )))
    }
}
 */
