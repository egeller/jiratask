//
//  AccountsView.swift
//  JiraTask
//
//  Created by Elena Geller on 05.04.20.
//  Copyright © 2020 Elena Geller. All rights reserved.
//

import SwiftUI
import Combine

struct AccountsView: View {
    public let willChange = PassthroughSubject<JiraSettingsViewModel, Never>()
    
    @ObservedObject var jiraSettingsViewModel:JiraSettingsViewModel
    @State var jiraAccounts: [JiraLogin]  = []
    
    var body: some View {
        NavigationView(){            
                    VStack(){
                        NavigationLink(destination: DashboardView(dashboardViewModel: DashboardViewModel(activeAccount : jiraSettingsViewModel.activeAccount ??  JiraLogin(login: "", password: "", serverUrl: "", name: "", id: "0")
                                                                                                        )), isActive: $jiraSettingsViewModel.testSuccessfull) {
                           EmptyView()
                            
                        }
        
             List(jiraAccounts){ account in
             Text(account.name).contextMenu {
                 Button(action: { self.login(account: account) } ) { HStack(){
                      Image(systemName: "chevron.right.square.fill")
                      Text(NSLocalizedString("Login", comment : "")) }
                     }
                 
                     
             }
         }
                         Spacer()
        }
       .onAppear{
             print("appear")
          
           jiraAccounts = jiraSettingsViewModel.accountsHolder.accounts
                    }
        }.navigationTitle("Jira Task")
            .toolbar {
                ToolbarItem(placement: .navigationBarTrailing) {
                    Button("Help") {
                        print("Help tapped!")
                    }
                }
            }
            
    }
        
    func login(account: JiraLogin){
        jiraSettingsViewModel.setActiveAccount(account: account)
        jiraSettingsViewModel.testAccount(account: account)
    }
}

struct AccountsView_Previews: PreviewProvider {
    static var previews: some View {
        AccountsView(jiraSettingsViewModel: JiraSettingsViewModel())
    }
}
