//
//  UnorderedLogsView.swift
//  JiraTask
//
//  Created by Elena Geller on 29.05.22.
//  Copyright © 2022 Elena Geller. All rights reserved.
//

import SwiftUI
import Realm
import RealmSwift

struct UnorderedLogsView: View {
    @ObservedResults(UndefinedWorklogObject.self) var worklogObjs
    let api = ServerApi.shared
    let dataService = DatabaseService.shared
    var jiraAccount: JiraLogin
    var projects: [Project]
    @State var chooseTaskForLog = false
    @State var currentLogObj: UndefinedWorklogObject? = nil
    @State private var showChooser = false
    @State var showFailDelete = false
    @State var showFailSave = false
    
    var body: some View {
        VStack{
            if worklogObjs.isEmpty {
                Text(NSLocalizedString("no worklogs", comment: ""))
                    .font(.system(size: 12))
                    .padding()
                          
            }
        List {
            ForEach(worklogObjs) { worklogObj in
                Text(worklogObj.begin.formatted() + " - " + (worklogObj.end?.formatted() ?? "")).font(.system(size: 12)).contextMenu {
                    Button(action: {
                        currentLogObj = worklogObj
                        showChooser = true
                    } ) { HStack(){
                        Image(systemName: "paperplane.fill")
                        Text(NSLocalizedString("Send", comment : "")) }
                    }
                    Button(action: {
                        deleteLog(worklogObj: worklogObj)
                    } ) { HStack(){
                        Image(systemName: "trash.square.fill")
                        Text(NSLocalizedString("Delete", comment : "")) }
                    }
                }
                
            }
        }
            NavigationLink(destination:  TaskChooseView(
                
                projects: projects,
                worklogObj: currentLogObj ?? UndefinedWorklogObject(),
                jiraAccount: jiraAccount
                               
            ),
            isActive: $showChooser){
            EmptyView()
        }
            .popover(isPresented: $showFailDelete){
                Text(NSLocalizedString("fail delete", comment: ""))
            }
       
        }.task{
            print("projects in logsview \(projects)")
        }
    }
   
    
    func deleteLog(worklogObj: UndefinedWorklogObject) {
        print("delete")
        print(worklogObj)
        let realm = try!  Realm()
        
         let logs = realm.objects(UndefinedWorklogObject.self)
            let currentLog = logs.where {
                $0.account == jiraAccount.name &&
                $0._id == worklogObj._id
            }
            if !Array(currentLog).isEmpty {
                do{
                try realm.write {
                    realm.delete(logs)
                }
                } catch {
                    showFailDelete = true
                }
            }
            
    }
    
    
    
}
/*
 struct UnorderedLogsView_Previews: PreviewProvider {
 static var previews: some View {
 UnorderedLogsView()
 }
 }*/
