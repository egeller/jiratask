//
//  DashboardView.swift
//  JiraTask
//
//  Created by Elena Geller on 04.04.20.
//  Copyright © 2020 Elena Geller. All rights reserved.
//

import SwiftUI
import SunburstDiagram
//import SVGWebView
import WebKit

struct DashboardView: View {
    let api = ServerApi.shared
    let dataService = DatabaseService.shared
    //@ObservedObject var jiraSettingsViewModel:JiraSettingsViewModel
    @ObservedObject var dashboardViewModel: DashboardViewModel
    
    @State private var showingLegend = false
    @State private var toProjects = false
    @State private var toWorklogs = false
    @State private var worklogStarted = false
    @State private var buttonText = "Start worklog"
    @State private var currentLog: UndefinedWorklog? = nil
    
    @State private var orientation = UIDeviceOrientation.unknown
    
    let diagramColors = [
        UIColor.systemBlue,
        UIColor.systemRed,
        UIColor.systemCyan,
        UIColor.orange,
        UIColor.systemBrown,
        UIColor.green
    ]
    @State var conf = SunburstConfiguration(nodes: [
    ])
    
    @State var spentTimesProject:[String : ProjectSpentTime] = [:]
    private var idiom : UIUserInterfaceIdiom { UIDevice.current.userInterfaceIdiom }
    
    @MainActor
    fileprivate func setDiagramAndLegendConfig(_ spentTimesProject: [String : ProjectSpentTime]) async {
        print(spentTimesProject)
        var nodes : [Node] = []
        var spent = 0.0
        var colorIndex = 0
        
        
        for (projectKey, spentTimes) in spentTimesProject {
            print(spentTimes)
            
            
            spent = spent + Double(spentTimes.time) / (3600.0 )
            print(Double(spentTimes.time) / ( 3600.0 ))
            let node = Node(name: projectKey,
                            showName: true,
                            image: nil, //spentTimes.avatar, //SVGWKWebView(svg: spentTimes.avatar ?? "", width : 48).snapshot(),
                            value: Double(spentTimes.time) / (3600.0 ),
                            backgroundColor: self.diagramColors[colorIndex])
            nodes.append(node)
            colorIndex = colorIndex + 1
        }
        
        let restSpent = 8 - spent
        print(restSpent)
        print(spent)
        
        let node = Node(name: "",
                        showName: true,
                        image: nil,//UIImage(named: "walking"),
                        value: Double(restSpent ),
                        backgroundColor: .lightGray)
        nodes.append(node)
        
        
        self.conf = SunburstConfiguration(nodes: nodes)
        self.conf.innerRadius = UIScreen.main.bounds.size.height/8 //60.0
        
        
        self.conf.calculationMode = .parentDependent(totalValue: 8.0)
    }
    
    func newWorklog() {
        
    }
    func showWorklogs() {
        toWorklogs = true
    }
    func showProjects() {
        toProjects = true
    }
    
    var body: some View {
        //NavigationView{
        VStack{
            HStack{
                Text("Dashboard")
                legendButton
                NavigationLink(destination: ProjectsView(projectsViewModel: ProjectsViewModel()), isActive: $toProjects) { EmptyView() }
                
                NavigationLink(destination: UnorderedLogsView( jiraAccount: dashboardViewModel.activeAccount ?? dashboardViewModel.dummyAccount, projects: dashboardViewModel.projects), isActive: $toWorklogs) { EmptyView() }
                
            }
            
            if orientation.isPortrait || orientation.isFlat ||
                idiom == .pad ||
                idiom == .mac {
                
                VStack(spacing: 0) {
                    SunburstView(configuration: self.conf)
                        /* .frame(width: UIScreen.main.bounds.size.height/2, height: UIScreen.main.bounds.size.height/2 )*/
                    Divider()
                        .edgesIgnoringSafeArea(.all)
                    //legendButton
                    Button(buttonText, action: processWorklog)
                }
                
                
            } else {
                
                VStack(spacing: 0) {
                    SunburstView(configuration: self.conf)
                        
                        .edgesIgnoringSafeArea(.all)
                        /*.frame(width: UIScreen.main.bounds.size.height/2,
                               height: UIScreen.main.bounds.size.height/2 )*/
                    Divider()
                        .edgesIgnoringSafeArea(.all)
                    //legendButton
                    Button(buttonText, action: processWorklog)
                }
                
            }
        //}
        }
        .toolbar {
            ToolbarItem {
                Menu {
                    //Button("New worklog", action: newWorklog)
                    Button(NSLocalizedString("Worklogs", comment: ""), action: showWorklogs)
                    if !toProjects {
                        Button(NSLocalizedString("Projects", comment: ""), action: showProjects)
                    }
                    if toProjects {
                        Button("Dashboard", action: {toProjects = false})
                    }
                    
                } label: {
                    Label("", systemImage: "line.3.horizontal")
                }
            }
        }
        .onAppear {
            
            orientation = UIDevice.current.orientation
            print(UIDevice.current.orientation.rawValue)
        }
        .onRotate { newOrientation in
            print(newOrientation.rawValue)
            print(UIDevice.current.orientation)
                    orientation = newOrientation
            if orientation.isPortrait || idiom == .pad || idiom == .mac {
                self.conf.innerRadius =  UIScreen.main.bounds.size.height/8//60.0
                self.conf.expandedArcThickness = UIScreen.main.bounds.size.height/8  // 60.0
            } else {
                self.conf.innerRadius =  UIScreen.main.bounds.size.height/8 //40.0
                self.conf.expandedArcThickness =  UIScreen.main.bounds.size.height/8 //40.0
            }
                }
        //.navigationBarBackButtonHidden(toProjects)
        .task {
            api.setAccount(dashboardViewModel.activeAccount!)
            self.spentTimesProject = await dashboardViewModel.getProjectsSpentTimes()
            await setDiagramAndLegendConfig(spentTimesProject)
            let currentLogObject = dataService.findLastUnorderedWorklogForAccount(jiraAccount: dashboardViewModel.activeAccount ?? dashboardViewModel.dummyAccount)
            if currentLogObject != nil {
                currentLog = UndefinedWorklog(from: currentLogObject!)
                print("currentlog in View \(currentLog)")
                worklogStarted = true
                buttonText = "Stop worklog"
            }
        }
        
        
        
    }
    
    var legendButton: some View {
        Button {
            showingLegend = true
        }
    label: {
        Image(systemName: "info.circle.fill")
    }//info.circle.fill
    .popover(isPresented: $showingLegend) {
        legendView
    }
    }
    
    var legendView: some View {
        VStack {
            // Text("Legend")
            List {
                ForEach(self.spentTimesProject.keys.sorted(), id: \.self) {key in
                    HStack {
                        SVGWKWebView (svg: spentTimesProject[key]?.avatar ?? "", width: 48.0)
                            .scaleEffect(0.5)
                        
                        Text(key)
                            .font(.system(size: 14))
                        Text(FormatHelper.formatSpentTime(Double(self.spentTimesProject[key]?.time ?? 2)) + " h")
                            .font(.system(size: 14))
                        Spacer()
                        
                    }.padding(5)
                }
            }
            .frame(width: UIScreen.main.bounds.size.width * 0.8 )
        }
    }
    
    func processWorklog() {
        if !worklogStarted {
            worklogStarted = true
            print("worklog starting")
            
            if currentLog == nil {
                dataService.saveUnorderedWorklogAtStart(jiraAccount: dashboardViewModel.activeAccount ?? dashboardViewModel.dummyAccount)
 
                let currentLogObject = dataService.findLastUnorderedWorklogForAccount(jiraAccount: dashboardViewModel.activeAccount ?? dashboardViewModel.dummyAccount)
                if currentLogObject != nil {
                    currentLog = UndefinedWorklog(from: currentLogObject!)
                }
  
                
            }
            
            
            buttonText = "Stop worklog"
        } else {
            worklogStarted = false
            buttonText = "Start worklog"
            print("worklog stopping")
            if currentLog != nil {
                guard let currentLogObject =  dataService.findLastUnorderedWorklogForAccount(jiraAccount: dashboardViewModel.activeAccount ?? dashboardViewModel.dummyAccount) else { return  }
                dataService.closeUnorderedWorklog(worklog: currentLogObject )
            }
            
        }
    }
}



 struct DashboardView_Previews: PreviewProvider {
 static var previews: some View {
 DashboardView(dashboardViewModel: DashboardViewModel(activeAccount :  JiraLogin(login: "", password: "", serverUrl: "", name: "", id: "0")
 ))
 }
 }

