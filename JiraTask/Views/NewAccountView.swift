//
//  NewAccountView.swift
//  JiraTask
//
//  Created by Elena Geller on 07.04.20.
//  Copyright © 2020 Elena Geller. All rights reserved.
//

import SwiftUI

struct NewAccountView: View {
    @State internal var accountName: String = "";
    @State internal var loginUsername: String = "";
    @State internal var jiraUrl: String = "";
    @State internal var jiraPassword: String = "";
    @State internal var passwordFieldIsSecure: Bool = true;
    @ObservedObject var jiraSettingsViewModel:JiraSettingsViewModel
    @State internal var accountSaved = false;
    @State internal var failText: String = "";
    
    var body: some View {
        VStack(){
            Form(){
                Section(header: Text(NSLocalizedString("Account Name", comment: ""))){
                    TextField(NSLocalizedString("enter account name", comment: ""), text :$accountName)
                }
                Section(header: Text(NSLocalizedString("Login Username or E-Mail", comment: ""))){
                    TextField(NSLocalizedString("enter login", comment: ""), text :$loginUsername)
                }
                Section(header: Text(NSLocalizedString("Jira Url", comment: ""))){
                    TextField(NSLocalizedString("enter jira url", comment: ""), text :$jiraUrl)
                }
                Section(header: Text(NSLocalizedString("Jira Password", comment: ""))){
                    if passwordFieldIsSecure{
                        HStack(){
                            SecureField(NSLocalizedString("enter password", comment: ""), text :$jiraPassword)
                        Button(action: {
                            self.changePasswordFieldSecurityStatus(securityStatus: false)
                        }  ){Image(systemName: "eye")}
                        }
                        
                    } else
                    {
                    HStack(){
                        TextField(NSLocalizedString("enter password", comment: ""), text :$jiraPassword)
                    Button(action: {
                        self.changePasswordFieldSecurityStatus(securityStatus: true)
                    }  ){Image(systemName: "eye.slash")}
                        }
                }
            }
                Section(){
                    Text(failText).alert(isPresented: $jiraSettingsViewModel.testFails) {
                        let alertText = "Test fails"
                        DispatchQueue.main.async{
                            self.failText = alertText;
                           
                            
                        }
                        
                        return Alert(title: Text("New Jira Account"), message: Text(alertText), dismissButton: .default(Text("OK"), action: {
                                
                                
                            }))
                            
                    }
                /*Button(action : {self.testConnection()}){
                    Text(NSLocalizedString("Test connection", comment: "")).font(.subheadline).padding()
                }.alert(isPresented: $jiraSettingsViewModel.testRun ) {
                    var alertText = "Test fails"
                    //jiraSettingsViewModel.testRun = false
                    if jiraSettingsViewModel.testSuccessfull {
                        alertText = "Test successfull"
                    }
                    
                    return Alert(title: Text("New Jira Account"), message: Text(alertText), dismissButton: .default(Text("OK")))
                    
                }*/
                
                Button(action : {self.testConnection()}){
                    Text(NSLocalizedString("Save account", comment: "")).font(.subheadline).padding()
                    }.alert(isPresented: $jiraSettingsViewModel.testSuccessfull){
                        
                        
                        DispatchQueue.main.async{
                            self.failText = "";
                            self.saveAccount()
                            self.loginUsername = "";
                            self.jiraUrl = ""
                            self.jiraPassword = ""
                            self.accountName = ""
                            
                            
                        }
                        return Alert(title: Text("New Jira Account"), message: Text("Account saved"), dismissButton: .default(Text("OK"), action: {
                        }))
                    }
                        
                    
                }
            }
        
        }
    }
            
 
    
    private func changePasswordFieldSecurityStatus(securityStatus: Bool){
        self.passwordFieldIsSecure = securityStatus;
    }
    
    private func testConnection(){
        self.failText = "";
        print("test connection")
        //let account = JiraLogin(login: "elena.geller@gmail.com", password: "sBrPS9utGkOdF2nP9AlKB6AB", serverUrl: "https://egeller.atlassian.net", name: "My Jira", id: "egeller@jiratask");
        let account = JiraLogin(login: loginUsername, password: jiraPassword, serverUrl: jiraUrl, name: accountName, id: loginUsername + "@" + jiraUrl);
        
        jiraSettingsViewModel.testAccount(account: account);
    
    }
    
    private func saveAccount(){
        print("save account")
        if(!accountSaved){
            print("to save")
            let account = JiraLogin(login: loginUsername, password: jiraPassword, serverUrl: jiraUrl, name: accountName, id: loginUsername + "@" + jiraUrl);
                   jiraSettingsViewModel.addAccount(account: account)
            DispatchQueue.main.async{
                self.accountSaved = true;
            }
                   
            
            
        }else{
            print("simple return");
            return
        }
       
        
        
    }
}

struct NewAccountView_Previews: PreviewProvider {
    static var previews: some View {
        NewAccountView(jiraSettingsViewModel: JiraSettingsViewModel())
    }
}
