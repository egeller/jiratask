//
//  JiraAccountsView.swift
//  JiraTask
//
//  Created by Elena Geller on 07.04.20.
//  Copyright © 2020 Elena Geller. All rights reserved.
//

import SwiftUI

struct JiraAccountsView: View {
    @State private var showActionSheet = false
    @ObservedObject var jiraSettingsViewModel:JiraSettingsViewModel
    
    let accounts = [JiraLogin(login : "login1", password: "psw1", serverUrl: "https://egeller.atlassian.net", name : "My personal Jira", id : "https://egeller.atlassian.net@login1" ) ]
    
    var body: some View {
        //NavigationView {
            VStack(){
           NavigationLink(destination: NewAccountView(jiraSettingsViewModel: jiraSettingsViewModel)) {
            Text(NSLocalizedString("New Account", comment: "")).font(.subheadline).padding()
           }.buttonStyle(PlainButtonStyle()).navigationBarTitle(NSLocalizedString("Accounts", comment: ""))
            
                List(jiraSettingsViewModel.jiraAccounts){ account in
                Text(account.name).contextMenu {
                    Button(action: { self.editAccount(account: account) } ) { HStack(){
                         Image(systemName: "pencil")
                         Text(NSLocalizedString("Edit", comment : "")) }
                        }
                    Button(action: { self.removeAccount(account: account) } ) {
                        HStack(){
                            Image(systemName: "trash")
                            Text(NSLocalizedString("Remove", comment: "")) }
                        }
                        
                }.actionSheet(isPresented: self.$showActionSheet) {
                    ActionSheet(
                        title: Text(""),
                        message: Text(""),
                        buttons: [
                            .cancel { print(self.showActionSheet) },
                            //.default(Text("Action")),
                            .destructive(Text(NSLocalizedString("Remove", comment: ""))){
                                print("Delete")
                                DispatchQueue.main.async{
                                    self.jiraSettingsViewModel.removeAccount(account: account)
                                }
                            }
                        ]
                    )
                }
            }
                Spacer()
            
        //}.navigationBarTitle(Text("Settings"))
        }
        
    }
    
    private func editAccount(account: JiraLogin){
        print(account)
    }
    private func removeAccount(account: JiraLogin){
        self.showActionSheet = true
        print(account)
        //jiraSettingsViewModel.removeAccount(account: account)
    }
}

struct JiraAccountsView_Previews: PreviewProvider {
    static var previews: some View {
        JiraAccountsView(jiraSettingsViewModel: JiraSettingsViewModel())
    }
}

