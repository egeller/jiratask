//
//  InfoView.swift
//  JiraTask
//
//  Created by Elena Geller on 11.04.20.
//  Copyright © 2020 Elena Geller. All rights reserved.
//

import SwiftUI

struct InfoView: View {
    var body: some View {
        VStack{
            HStack{
                Spacer()
                Text(self.getVersion()).padding().font(.subheadline).multilineTextAlignment(.trailing)
            }
        HTMLRepresentedView()
        }
    }
    
    func getVersion()->String{
        return Bundle.main.versionString;
    }
}

struct InfoView_Previews: PreviewProvider {
    static var previews: some View {
        InfoView()
    }
}
