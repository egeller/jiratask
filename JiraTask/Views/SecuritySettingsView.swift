//
//  SecuritySettingsView.swift
//  JiraTask
//
//  Created by Elena Geller on 07.04.20.
//  Copyright © 2020 Elena Geller. All rights reserved.
//

import SwiftUI

struct SecuritySettingsView: View {
    
    let settingsHelper = SettingsHelper();
    
    
    @ObservedObject var securitySettingsViewModel: SecuritySettingsViewModel
    //@State var isBiometricNeeded: Bool = security
    
    
    var body: some View {
        VStack(){
            
            Toggle(isOn: self.$securitySettingsViewModel.isBiometricNeeded) {
                Text(NSLocalizedString("Biometric Authenication required", comment: "")).font(.subheadline).padding()
                
                if (self.securitySettingsViewModel.isBiometricNeeded) {
                    Text("\(self.toggleAction(state: "Checked"))")
                } else {
                    
                    Text("\(self.toggleAction(state: "Unchecked"))")
                }
            }
            Spacer()
        }.padding()
    }
    func toggleAction(state: String) -> String {
        print("The switch  is \(state)")
        
        securitySettingsViewModel.saveBiometricNeeded()
        return ""
    }
    
}



struct SecuritySettingsView_Previews: PreviewProvider {
    static var previews: some View {
        SecuritySettingsView(securitySettingsViewModel: SecuritySettingsViewModel())
    }
}
