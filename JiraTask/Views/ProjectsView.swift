//
//  ProjectsView.swift
//  JiraTask
//
//  Created by Elena Geller on 08.05.22.
//  Copyright © 2022 Elena Geller. All rights reserved.
//

import SwiftUI

struct ProjectsView: View {
    @ObservedObject var projectsViewModel: ProjectsViewModel
    
    
    var body: some View {
        VStack{
            List(projectsViewModel.projects, id: \.self) { project in
                NavigationLink(destination: ProjectTasksView(projectViewModel: ProjectViewModel(project: project))) {
                    Text(project.name)
                    
                }
                
            }
        }
        .task {
            do {
                _ = try await projectsViewModel.getProjects()
            }catch {
                ///
            }
        }
        
        
    }
}

/*
 struct ProjectsView_Previews: PreviewProvider {
 static var previews: some View {
 ProjectsView(projectsViewModel: ProjectsViewModel())
 }
 } */
