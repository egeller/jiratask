//
//  TaskChooseView.swift
//  JiraTask
//
//  Created by Elena Geller on 29.05.22.
//  Copyright © 2022 Elena Geller. All rights reserved.
//

import SwiftUI
import Combine
import Realm
import RealmSwift

struct TaskChooseView: View {
    @Environment(\.dismiss) var dismiss
    var projects: [Project]
    
    var worklogObj:UndefinedWorklogObject?
    var jiraAccount: JiraLogin
    var projectKeys: [String] = []
    @State var projectTasksKeys: [String] = []
    @State var projectsWithKeys: Dictionary<String, Project> = [:]
    @State var tasksWithKeys: [String:Task] = [:]
    @State private var selectedProjectKey: String = ""
    @State private var selectedTaskKey: String = ""
    @State private var saveFail = false
    
    
    private var api = ServerApi.shared
    private var dataService = DatabaseService.shared
    let dummyProject = Project(key: "0", name: "Dummy", id: "Dummy", avatarUrls: AvatarUrls(m48: "", m24: ""))
    
    
    public init (
        
        projects: [Project],
        worklogObj: UndefinedWorklogObject,
        jiraAccount: JiraLogin
        
        
    )
    {
        //self.rootActive = rootActive
        print("init chooser")
        self.worklogObj = worklogObj
        self.jiraAccount = jiraAccount
        self.projects = projects
        //
        self.projectKeys = projects.map{$0.key}
        
        //let selectedProject = projects.first!
        self.selectedProjectKey = ""
        
        
        print("projects in taskchoose \(projects)")
        print(projectKeys)
        
        
    }
    private func string(from item: Project) -> String {
        return item.key
    }
    
    var body: some View {
        VStack{
            Form {
                Section{
                    
                    Picker(NSLocalizedString("Please choose a project", comment: ""), selection: $selectedProjectKey) {
                        ForEach(projectKeys, id: \.self) {
                            Text($0)
                        }
                    }
                    .font(.system(size: 14))
                    .onChange(of: selectedProjectKey, perform: { (selectedKey) in
                        print("selected key \(selectedKey)")
                        
                        async{
                            do {
                                let projectTasks = try await api.getProjectTasksByProjectKey(projectKey: selectedKey)
                                projectTasksKeys = projectTasks.map{$0.key}
                            } catch {
                                
                            }
                        }
                    })
                }
                Section {
                    Picker(NSLocalizedString("Please choose a task", comment: ""), selection: $selectedTaskKey) {
                        ForEach(projectTasksKeys, id: \.self) {
                            Text($0)
                        }
                    }
                    .font(.system(size: 14))
                    .onChange(of: selectedTaskKey, perform: { (selectedTKey) in
                        print(selectedTKey)
                    })
                }
                
                
                
                
                
                Text(NSLocalizedString("You selected", comment: "") + ": \(selectedProjectKey )  \(selectedTaskKey ?? "no")")
                    .font(.system(size: 14))
                
                Section {
                    Button(NSLocalizedString("Send log", comment: ""), action: sendLog
                           
                    )
                }
                .popover(isPresented: $saveFail){
                    Text(NSLocalizedString("fail send", comment: "")).font(.system(size: 14))
                }
            }
        }
        
        
        
    }
    
    func getProjectTasks(selectedKey: String) async {
        do {
            let projectTasks = try await api.getProjectTasksByProjectKey(projectKey: selectedKey)
            projectTasksKeys = projectTasks.map{$0.key}
        } catch {
            
        }
    }
    
    @MainActor
    func sendLog() {
        
        async {
            
            let isSent = try await api.sendWorklog(
                issueKey: selectedTaskKey,
                begin: worklogObj?.begin ?? Date(),
                end: worklogObj?.end ?? Date()
            )
            if !isSent {
                saveFail = true
                return
            }
            let realm = try! await  Realm()
            if isSent && worklogObj != nil {
                let logs = realm.objects(UndefinedWorklogObject.self)
                let currentLog = logs.where {
                    $0.account == jiraAccount.name &&
                    $0._id == worklogObj?._id ?? "0"
                }
                if !Array(currentLog).isEmpty {
                    do {
                    try realm.write {
                    realm.delete(logs)
                    }
                    }catch {
                        print ("fail delete")
                    }
                }
                //print("we have current \(Array(currentLog)[0])")
                //return Array(currentLog)[0]
                
            }
            dismiss()
        }
        
    }
    
    
    
    
}

/*
 struct TaskChooseView_Previews: PreviewProvider {
 static var previews: some View {
 TaskChooseView()
 }
 }
 */
