//
//  SettingsView.swift
//  JiraTask
//
//  Created by Elena Geller on 07.04.20.
//  Copyright © 2020 Elena Geller. All rights reserved.
//

import SwiftUI

struct SettingsView: View {
    @ObservedObject var securitySettingsViewModel: SecuritySettingsViewModel
    @ObservedObject var jiraSettingsViewModel: JiraSettingsViewModel
    
    var body: some View {
        NavigationView {
            VStack(){
           NavigationLink(destination: SecuritySettingsView(securitySettingsViewModel: securitySettingsViewModel)) {
            Text(NSLocalizedString("Security Settings", comment: "")).font(.subheadline).padding()
           }.buttonStyle(PlainButtonStyle())
            
                NavigationLink(destination: JiraAccountsView(jiraSettingsViewModel: JiraSettingsViewModel())) {
                Text(NSLocalizedString("Jira Accounts", comment: "")).font(.subheadline).padding()
            }.buttonStyle(PlainButtonStyle())
                Spacer()
             }
            
        }.navigationBarTitle(Text(NSLocalizedString("Settings", comment: "")))

    }
}

struct SettingsView_Previews: PreviewProvider {
    static var previews: some View {
        SettingsView(securitySettingsViewModel: SecuritySettingsViewModel(), jiraSettingsViewModel: JiraSettingsViewModel())
    }
}
