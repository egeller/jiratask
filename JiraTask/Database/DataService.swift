//
//  DataService.swift
//  JiraTask
//
//  Created by Elena Geller on 28.05.22.
//  Copyright © 2022 Elena Geller. All rights reserved.
//

import Foundation
import Realm
import RealmSwift

@MainActor
class DatabaseService {
    
    static let shared = DatabaseService()
    
    let realm = try! Realm()
    
    func saveUnorderedWorklogAtStart(jiraAccount: JiraLogin) {
        let log = UndefinedWorklogObject()
        log.account = jiraAccount.name
        log.begin = Date()
        log.isSent = false
        log.end = nil
        log._id = UUID().uuidString
        try! realm.write {
            realm.add(log)
        }
        
    }
    
    
    func findLastUnorderedWorklogForAccount(jiraAccount: JiraLogin) -> UndefinedWorklogObject? {
        
        let logs = realm.objects(UndefinedWorklogObject.self)
        print("logs: \(logs)")
        let currentLog = logs.where {
            $0.account == jiraAccount.name && $0.end == nil
        }
        print("current logs: \(Array(currentLog))")
        if Array(currentLog).isEmpty {
            return nil
        }
        print("we have current \(Array(currentLog)[0])")
        return Array(currentLog)[0]
    }
    
    func getUnorderedWorklogsForAccount(jiraAccount: JiraLogin) -> [UndefinedWorklogObject] {
        
        let logs = realm.objects(UndefinedWorklogObject.self)
        let currentLog = logs.where {
            $0.account == jiraAccount.name
        }
        return Array(currentLog)
        
    }
    
    func getClosedUnorderedWorklogsForAccount(jiraAccount: JiraLogin) -> [UndefinedWorklogObject] {
        
        let logs = realm.objects(UndefinedWorklogObject.self)
        let currentLog = logs.where {
            $0.account == jiraAccount.name && $0.end != nil
        }
        return Array(currentLog)
        
    }
    
    func closeUnorderedWorklog(worklog: UndefinedWorklogObject) {
        try! realm.write {
            //Enddate setzen
            worklog.end = Date()
            
        }
    }
    
    func markSent(worklog: UndefinedWorklogObject) {
        try! realm.write {
            //Enddate setzen
            worklog.isSent = true
            
        }
    }
    
    func deleteUnorderedWorklog(worklog: UndefinedWorklogObject) {
        do {
            
            try realm.write {
                realm.delete(worklog)
            }
        } catch {
            print(error)
        }
    }

}
