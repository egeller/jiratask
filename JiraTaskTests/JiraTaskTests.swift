//
//  JiraTaskTests.swift
//  JiraTaskTests
//
//  Created by Elena Geller on 04.04.20.
//  Copyright © 2020 Elena Geller. All rights reserved.
//

import XCTest
@testable import JiraTask

class JiraTaskTests: XCTestCase {

    override func setUpWithError() throws {
        // Put setup code here. This method is called before the invocation of each test method in the class.
    }

    override func tearDownWithError() throws {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
    }
/*
    func testExample() throws {
        // This is an example of a functional test case.
        // Use XCTAssert and related functions to verify your tests produce the correct results.
    }
 */
    
    func testPathHelper() throws{
        let pathHelper = RequestPathHelper();
        pathHelper.readPathJson();
        print(pathHelper.jiraUriCollection);
        let projectPath = pathHelper.readPathParametersFor(reqName: "projects");
        print(projectPath);
        XCTAssertEqual(projectPath.method, "GET");
    }
   /*
    func testAccount() throws{
        let account = JiraLogin(login: "elena.geller@gmail.com", password: "sBrPS9utGkOdF2nP9AlKB6AB", serverUrl: "https://egeller.atlassian.net", name: "My Jira", id: "1");
        let jiraSettingsViewModel = JiraSettingsViewModel();
        jiraSettingsViewModel.testAccount(account: account);
        XCTAssertEqual(jiraSettingsViewModel.testSuccessfull, true);
    }*/

    func testPerformanceExample() throws {
        // This is an example of a performance test case.
        self.measure {
            // Put the code you want to measure the time of here.
        }
    }

}
